﻿## Generation of self-organizing patterns in nonlinear dynamical systems and their exploitation in order to hide secret visual information

#### Authorship

Author: Pranas Ziaukas

Supervisor: prof. habil. dr. Minvydas Ragulskis. 

The Faculty of Mathematics
and Natural Sciences, Kaunas University of Technology.

#### Metadata

Research area and field
* physical sciences
* mathematics

Key words 
* self-organization
* dynamical systems
* patterns
* visual cryptography
* steganography

Kaunas, 2014. 49 p.

#### Summary 

Since the usage of modern computing became so prevalent the world of visual cryptography, ste-
ganography and hidden information altogether has changed substantially. The techniques and algorithms
are becoming more sophisticated as the interest from the scientific community grows and more powerful
tools are easily available. Precisely to meet the increasing needs this bachelor thesis titled ‘Generation
of Self-Organizing Patterns in Nonlinear Dynamical Systems and Their Exploitation in Order to Hide
Secret Visual Information’ was prepared.

A multitude of steganographic visual communication schemes is proposed in this work. All the
schemes have one essential property in common - self-organizing patterns induced by complex interac-
tions between locally competing abstract elements are exploited for hiding and transmitting secret visual
information. Other that that it is shown that very different underlying principles may lead to the pheno-
menon of self organization and consequently satisfying results.

Properties of the proposed communication systems such as hiding capacity (in terms of minimal
size of hidden elements and minimal recognizable distances between components), the sensitivity to
perturbations of private and public keys, the robustness against specific attacks are discussed in details.
Finally several computational experiments are used in order to demonstrate effectiveness, usability and
efficiency of the proposed communication schemes in adequate situations.
