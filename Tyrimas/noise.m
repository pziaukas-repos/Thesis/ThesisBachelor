function [ M ] = noise( N, seed )
% Generates a matrix of pseudo-uniformly distributed values from interval [0;1].
% N - dimensions of the matrix;
% seed - initial value;

M(prod(N)) = seed;

for k = prod(N)-1:-1:1
    M(k) = 4 * M(k+1) * (1 - M(k+1));
end;
M = reshape(M,N);

end

