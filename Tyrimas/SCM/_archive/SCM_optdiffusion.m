param_min = [0 0 0];
Smin = 40000;

for alpha = .05:.05:1;
for gamma = 0:.01:.1;
DXX = XX;
for iter = 1:50;
    DXX = diffuse(DXX,alpha,1);
    FXX = bwperim(DXX > gamma);
    bw1 = DXX > gamma;
    [L,num] = bwlabel( ~bw1 );
    counts = sum(bsxfun(@eq,L(:),1:num));
    [~,ind] = max(counts);
    bw2 = ~(L==ind);
    [L,num] = bwlabel( bw1 );
    if ~nnz(bw1~=bw2) && num==1;
        if (sum(FXX(:)) <= Smin) && (sum(bw1(:)) > 400) && (sum(bw1(:)) < 6000);
            Smin = sum(FXX(:));
            param_min = [alpha iter gamma];
        end;
    end;
end;
end;
end;
param_min
%imshow(DXX=diffuse(XX,.15,17)>0.1);
%diffuse(XX,.1,28)>.1