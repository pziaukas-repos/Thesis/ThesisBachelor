M = noise([200 200], x0);
fM = competitive(M,l,a,b,iter);

X = zeros(200);
for k = 1:5;
    t = 101-round(k/2):100-round(k/2)+k;
    X(t,t) = 1;
    MX = M + delta * X;
    fMX = competitive(MX,l,a,b,iter);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('properties/prim%d',k));
end;

for k = 1:2;
for r = 1:3:16;
    X = zeros(200);
    t = 101-round(k/2):100-round(k/2)+k;
    X(t-round(r/2)-round(k/2), t) = 1;
    X(t-round(r/2)-round(k/2)+r+k, t) = 1;
    MX = M + delta * X;
    fMX = competitive(MX,l,a,b,iter);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('properties/dist%d_%d',k,r));
end;
end;

