function fM = competitive( M, l, a, b, n )
% Evolution of matrix M;
% l, a, b - given parameters;
% n - number of iterations;
% Maynard-Smith map

if (n <= 0)
    %MM = frame(M); % Adds a thickness 1 black frame
    return;
end;

N = size(M);

for i = N(1):-1:1;
    for j = N(2):-1:1;
        K = -M(i,j);
        for p = -2:0;
            for q = -2:0;
                K = K + M(mod(i+p,N(1))+1,mod(j+q,N(2))+1);
            end;
        end;
        fM(i,j) = l * M(i,j) / (1 + (M(i,j) + a*K)^b);
    end;
end;

if (n > 1) % Use > instead of >= when no frame is required
    fM = competitive( fM, l, a, b, n-1 );
end;

end