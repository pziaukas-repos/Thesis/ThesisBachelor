l = 5;
b = 4;
a = .3;
x0 = .05;
delta = .01;
iter = 25;

M = noise([200 200],x0);
X = double(imread('../E_sk.png'))>1;
% X = zeros(200);
% X(101,101) = 1;

MX = M + delta * X;
fM = competitive(M,l,a,b,iter);
fMX = competitive(MX,l,a,b,iter);
XX = abs(fM-fMX);