function [ MM ] = frame( M )
% Adds a thickness 1 black frame

MM = ones(size(M)+2);
MM(2:end-1,2:end-1) = M;

end

