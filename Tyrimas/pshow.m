function [ ] = pshow( M, label )
% Prints a matrix M

imshow(frame(M));
caxis([0 1]);
colormap(flipud(gray));
colorbar;
if (nargin == 2);
    set(gcf,'paperpositionmode','auto');
    print('-dpng',label);
end;
end

