function [ M ] = standartize( M )
% Linearly transforms data into interval [0;1]

M = (M-min(M(:)))/(max(M(:))-min(M(:)));

end

