function [ fM ] = social( M, R, S, T, P, c, n )
% Evolution of matrix M;
% R,S,T,P - given parameters;
% c - weight of current strategy;
% n - number of iterations;
% Average neighbor method

if (n <= 0)
    return;
end;

N = size(M);
w = [R S; T P];

for i = N(1):-1:1;
    for j = N(2):-1:1;
        W(i,j) = -w(M(i,j)+1,M(i,j)+1);
        for p = -2:0;
            for q = -2:0;
                W(i,j) = W(i,j) + w(M(i,j)+1,M(mod(i+p,N(1))+1,mod(j+q,N(2))+1)+1);
            end;
        end;
    end;
end;

for i = N(1):-1:1;
    for j = N(2):-1:1;
        Q = zeros(2);
        for p = -2:0;
            for q = -2:0;
                r = W(mod(i+p,N(1))+1,mod(j+q,N(2))+1);
                ind = M(mod(i+p,N(1))+1,mod(j+q,N(2))+1)+1;
                Q(ind,1) = Q(ind,1) + r;
                Q(ind,2) = Q(ind,2) + 1;
            end;
        end;
        Q(M(i,j)+1,1) = c*Q(M(i,j)+1,1);
        fM(i,j) = diff(Q(:,1)./max(Q(:,2),1)) >= 0;
    end;
end;

if (n > 1) && ~isequal(fM,M)
    fM = social( fM, R, S, T, P, c, n-1 );
end;
    
end