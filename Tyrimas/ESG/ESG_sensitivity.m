for d = 8:-1:1;
    fM = social(M,R+.1^d,S,T,P,c,iter);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('sensitivity/sens_R+%d',d));
    fM = social(M,R,S+.1^d,T,P,c,iter);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('sensitivity/sens_S+%d',d));
    fM = social(M,R,S,T+.1^d,P,c,iter);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('sensitivity/sens_T+%d',d));
    fM = social(M,R,S,T,P+.1^d,c,iter);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('sensitivity/sens_P+%d',d));
    fM = social(M,R,S,T,P,c,iter+d);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('sensitivity/sens_iter+%d',d));
end;

for d = 15:-1:15;
    M0 = noise([200 200],x0+.1^d)>.5;
    fM = social(M0,R,S,T,P,c,iter);
    XX = abs(fM-fMX);
    pshow(XX,sprintf('sensitivity/sens_x0+%d',d));
end;

fM = social(M,R,S,T,P,c,iter);
XX = abs(fM-fMX);