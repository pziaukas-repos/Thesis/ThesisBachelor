R = 3;
S = 2;
T = 4;
P = 0;
c = 1;
x0 = .05;
iter = 30;

M = noise([200 200],x0)>.5;
X = double(imread('../E_fl.png'))>1;

MX = abs(M-X);
fM = social(M,R,S,T,P,c,iter);
fMX = social(MX,R,S,T,P,c,iter);
XX = abs(fM-fMX);