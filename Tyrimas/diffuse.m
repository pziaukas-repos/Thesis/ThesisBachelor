function [ M ] = diffuse( M, alpha, iter )

N = size(M,1);


for t = 1:iter;
    for i = N:-1:1;
        for j = N:-1:1;
            K(i,j) = M(mod(i+N-2,N)+1,j) + M(i,mod(j+N-2,N)+1) + ...
                M(mod(i,N)+1,j) + M(i,mod(j,N)+1);
        end;
    end;
    M = M + alpha * (K-4*M);
end;

end

