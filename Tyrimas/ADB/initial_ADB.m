x0 = .05;
delta = 0.0005;
iter = 10000;
r = 0.5;
eps = 1.0;
beta = 0.6;
k = 2.6;
eta = 0.25;
w = 0.4;
b = 0.3154;

d = [0.01 0.0115; 0.01 1];

root = sqrt(k^2*(r*w*eps-eps*beta+eta)^2+4*r*k*w*eps*eta*b);
Nstar = (k*(r*w*eps-eps*beta+eta)+root)/(2*r*w*eps);
Pstar = (beta*eps-eta)*Nstar/(w*eta)-b/w;

M = noise([200 200],x0);
P(1:200,1:200) = Pstar + (M-.5)/1000;
N(1:200,1:200) = Nstar + (M-.5)/1000;

fN = predprey(N,P,d,iter);
X = imread('../E_sk.png')<100;
NX = N + delta*X;
fNX = predprey(NX,P,d,iter);
XX = standartize(fN-fNX);