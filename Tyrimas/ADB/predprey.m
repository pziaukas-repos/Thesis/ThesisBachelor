function [ N ] = predprey( N, P, d, iter )
% Predator-Prey model

% d11 = 0.01;
% d12 = 0.0115;
% d21 = 0.01;
% d22 = 1.00;
d11 = d(1,1);
d12 = d(1,2);
d21 = d(2,1);
d22 = d(2,2);
tau = 0.01;
h = 0.25;

r = 0.5;
eps = 1.0;
beta = 0.6;
k = 2.6;
eta = 0.25;
w = 0.4;
b = 0.3154;

for t = 1:iter;
    DN = imfilter(N,[0 1 0; 1 -4 1; 0 1 0],'circular');
    DP = imfilter(P,[0 1 0; 1 -4 1; 0 1 0],'circular');
    N = N + tau*d11*DN/h^2 + tau*d12*DP/h^2 + tau * (r*(1-N/k).*N - beta*N.*P./(b+N+w*P));
    P = P + tau*d21*DN/h^2 + tau*d22*DP/h^2 + tau * (eps*beta*N.*P./(b+N+w*P) - eta*P);
end